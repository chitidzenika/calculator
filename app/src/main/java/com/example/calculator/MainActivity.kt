package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

private lateinit var resultTextView: TextView
private var operand: Double = 0.0
private var operation: String = ""

class MainActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)

    }

    fun numberClick(view: android.view.View) {
        if (view is TextView) {
            val result: String = resultTextView.text.toString()
            val number: String = view.text.toString()
            val temp: String = ""
            if (result == "0")
                resultTextView.text =  number
            else
                resultTextView.text = result + number


        }
    }

    fun operationClick(view: android.view.View) {
        if (view is TextView) {
            val result = resultTextView.text.toString()
            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }
            operation = view.text.toString()
            resultTextView.text = ""


        }
    }
    fun equalsClick(view: android.view.View) {
        val secOperandText = resultTextView.text.toString()
        var secOperand =  0.0
        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()
        }
        when (operation) {
            "/" -> resultTextView.text = (operand / secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
        }
    }
    fun clearClick(view: android.view.View) {
        resultTextView.text = "0"
    }
    fun delClick(view: android.view.View) {
        if (resultTextView.text == "0")
            resultTextView.text = "0"
        else
        resultTextView.text = resultTextView.text.dropLast(1)
        if (resultTextView.text == "")
            resultTextView.text = "0"
    }

}






